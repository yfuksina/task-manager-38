package ru.tsc.fuksina.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.api.repository.IProjectRepository;
import ru.tsc.fuksina.tm.enumerated.Status;
import ru.tsc.fuksina.tm.model.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.UUID;

public class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @NotNull
    private final String TABLE_NAME = "TM_PROJECT";

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project fetch(@NotNull final ResultSet row) {
        @NotNull final Project project = new Project();
        project.setId(row.getString("id"));
        project.setName(row.getString("name"));
        project.setDescription(row.getString("description"));
        project.setStatus(Status.toStatus(row.getString("status")));
        project.setUserId(row.getString("user_id"));
        project.setCreated(row.getTimestamp("created_dt"));
        project.setDateStart(row.getTimestamp("start_dt"));
        project.setDateEnd(row.getTimestamp("end_dt"));
        return project;
    }

    @Nullable
    @Override
    public Project add(@NotNull final String userId, @NotNull final Project project) {
        project.setUserId(userId);
        return add(project);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project add(@NotNull final Project project) {
        @NotNull final String sql = "INSERT INTO " + getTableName()+
                " (id, created_dt, name, description, status, user_id, start_dt, end_dt) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, UUID.randomUUID().toString());
            statement.setTimestamp(2, new Timestamp(project.getCreated().getTime()));
            statement.setString(3, project.getName());
            statement.setString(4, project.getDescription());
            statement.setString(5, project.getStatus().toString());
            statement.setString(6, project.getUserId());
            if (project.getDateStart() != null) {
                statement.setTimestamp(7, new Timestamp(project.getDateStart().getTime()));
            } else statement.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
            if (project.getDateEnd() != null) {
                statement.setTimestamp(8, new Timestamp(project.getDateEnd().getTime()));
            } else statement.setTimestamp(8, new Timestamp(System.currentTimeMillis()));
            statement.executeUpdate();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project update(@NotNull final Project project) {
        @NotNull final String sql =
                "UPDATE " + getTableName()+ " SET name = ?, description = ?, status = ? WHERE id = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getName());
            statement.setString(2, project.getDescription());
            statement.setString(3, project.getStatus().toString());
            statement.setString(4, project.getId());
            statement.executeUpdate();
        }
        return project;
    }

}
