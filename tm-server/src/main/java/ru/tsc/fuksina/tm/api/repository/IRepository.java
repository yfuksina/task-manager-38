package ru.tsc.fuksina.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.enumerated.Sort;
import ru.tsc.fuksina.tm.model.AbstractModel;
import ru.tsc.fuksina.tm.model.Task;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@NotNull Comparator<M> comparator);

    @NotNull
    List<M> findAll(@NotNull Sort sort);

    @Nullable
    M add(@NotNull M model);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    @Nullable
    M findOneById(@NotNull String id);

    @Nullable
    M remove(@NotNull M model);

    @Nullable
    M removeById(@NotNull String id);

    @NotNull
    M update(@NotNull final M model);

    void removeAll(@NotNull Collection<M> collection);

    void clear();

    boolean existsById(String id);

    long getSize();

}
